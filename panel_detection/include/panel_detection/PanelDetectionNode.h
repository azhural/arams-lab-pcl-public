#ifndef PANELDETECTIONNODE_H
#define PANELDETECTIONNODE_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

class PanelDetectionNode
{
public:
  PanelDetectionNode(ros::NodeHandle nh, ros::NodeHandle priv_nh);
  ~PanelDetectionNode();

private:
  ros::NodeHandle nh;
  ros::NodeHandle priv_nh;

  std::string point_cloud_topic;

  pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud;

  // add subscribers here

  // add publishers here

  // callback declaration here

  // additional function declarations here
  void filterPCLCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud);
  void filterCloseDistanceInPCLCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                                     pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud);
  void filterFarDistanceInPCLCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                                   pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud);
  void separatePCLCloudByAngle(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>& output_clouds);
  void getClusteredCloudsFromPCLCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                                      std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>& clustered_clouds,
                                      int min_points, int max_points, float cluster_tolerance);
  void getClusterIndicesfromPCLCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                                     std::vector<pcl::PointIndices>& output_indices,
                                     int min_points, int max_points, float cluster_tolerance);
  double calculatePCLPointDistance(const pcl::PointXYZ& point1, const pcl::PointXYZ& point2);
};

#endif // PANELDETECTIONNODE_H
