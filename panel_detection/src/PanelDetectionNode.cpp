#include <geometry_msgs/PoseStamped.h>

#include <panel_detection/PanelDetectionNode.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/crop_box.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

PanelDetectionNode::PanelDetectionNode(ros::NodeHandle nh, ros::NodeHandle priv_nh)
  :nh{nh},
    priv_nh{priv_nh},
    point_cloud(new pcl::PointCloud<pcl::PointXYZ>)
{
  this->priv_nh.param("pointcloud_topic", point_cloud_topic, std::string("/velodyne_points"));

  // subscribe here
  // be aware, that we are using classes:
  // using member functions as callbacks is slightly different from how you saw it in the ROS tutorials
  // ------------------------------------


  // advertise here
  // ------------------------------------
}

PanelDetectionNode::~PanelDetectionNode(){
  ROS_INFO_STREAM("PanelDetectionNode Shutting Down Now!");
}


// callback definition here

void PanelDetectionNode::filterPCLCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud){
  pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_io_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  filterCloseDistanceInPCLCloud(input_cloud, tmp_io_cloud);
  filterFarDistanceInPCLCloud(tmp_io_cloud, input_cloud);
}

void PanelDetectionNode::filterCloseDistanceInPCLCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                                                       pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud){
  // http://docs.pointclouds.org/1.7.0/classpcl_1_1_crop_box.html
  pcl::CropBox<pcl::PointXYZ> cropbox_filter;
  Eigen::Vector4f minPt;
  Eigen::Vector4f maxPt;
  Eigen::Vector3f rotation;
  Eigen::Vector3f translation;

  // keep everything outside the box
  cropbox_filter.setNegative(true);

  // Placeholder, output cloud will be unfiltered
  // remove and apply the correct filter
  *output_cloud = *input_cloud;
}


void PanelDetectionNode::filterFarDistanceInPCLCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                                                     pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud){
  // http://docs.pointclouds.org/1.7.0/classpcl_1_1_crop_box.html
  pcl::CropBox<pcl::PointXYZ> cropbox_filter;
  Eigen::Vector4f minPt;
  Eigen::Vector4f maxPt;
  Eigen::Vector3f rotation;
  Eigen::Vector3f translation;

  // remove everything outside the box
  cropbox_filter.setNegative(false);

  // Placeholder, output cloud will be unfiltered
  // remove and apply the correct filter
  *output_cloud = *input_cloud;
}

void PanelDetectionNode::separatePCLCloudByAngle(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>& output_clouds){

  //seperating VLP16Cloud in 16 single point clouds (name of cloud equal to angle!)
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud15 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud13 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud11 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud9 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud7 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud5 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud3 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud1 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_1 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_3 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_5 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_7 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_9 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_11 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_13 (new pcl::PointCloud<pcl::PointXYZ>),
      cloud_15 (new pcl::PointCloud<pcl::PointXYZ>);

  //seperate the laser lines in single point clouds
  for(int i = 0; i < point_cloud->points.size() ; i++){
    pcl::PointXYZ pointT;
    double distance, angle;
    const double PI = 3.14159265358979323846;

    pointT= point_cloud->at(i);
    distance = sqrt(pointT.data[0]*pointT.data[0] +
        pointT.data[1]*pointT.data[1] +
        pointT.data[2]*pointT.data[2]);

    angle = asin(pointT.data[2]/distance);
    angle = angle*360/(2*PI);

    //real Velodyne
    if(angle > 0.5 && angle < 1.5)
      cloud1->push_back(pointT);
    else if(angle > 2.5 && angle < 3.5)
      cloud3->push_back(pointT);
    else if(angle > 4.5 && angle < 5.5)
      cloud5->push_back(pointT);
    else if(angle > 6.5 && angle < 7.5)
      cloud7->push_back(pointT);
    else if(angle > 8.5 && angle < 9.5)
      cloud9->push_back(pointT);
    else if(angle > 10.5 && angle < 11.5)
      cloud11->push_back(pointT);
    else if(angle > 12.5 && angle < 13.5)
      cloud13->push_back(pointT);
    else if(angle > 14.5 && angle < 15.5)
      cloud15->push_back(pointT);

    else if(angle < -0.5 && angle > -1.5)
      cloud_1->push_back(pointT);
    else if(angle < -2.5 && angle > -3.5)
      cloud_3->push_back(pointT);
    else if(angle < -4.5 && angle > -5.5)
      cloud_5->push_back(pointT);
    else if(angle < -6.5 && angle > -7.5)
      cloud_7->push_back(pointT);
    else if(angle < -8.5 && angle > -9.5)
      cloud_9->push_back(pointT);
    else if(angle < -10.5 && angle > -11.5)
      cloud_11->push_back(pointT);
    else if(angle < -12.5 && angle > -13.5)
      cloud_13->push_back(pointT);
    else if(angle < -14.5 && angle > -15.5)
      cloud_15->push_back(pointT);
  }

  //dont check it for angle 1° and -1°, otherwise code will crash if cloud is empty!!
  output_clouds.push_back(cloud1);
  output_clouds.push_back(cloud_1);

  if(cloud3->points.size() != 0)
    output_clouds.push_back(cloud3);
  if(cloud_3->points.size() != 0)
    output_clouds.push_back(cloud_3);

  if(cloud5->points.size() != 0)
    output_clouds.push_back(cloud5);
  if(cloud_5->points.size() != 0)
    output_clouds.push_back(cloud_5);

  if(cloud7->points.size() != 0)
    output_clouds.push_back(cloud7);
  if(cloud_7->points.size() != 0)
    output_clouds.push_back(cloud_7);

  if(cloud9->points.size() != 0)
    output_clouds.push_back(cloud9);
  if(cloud_9->points.size() != 0)
    output_clouds.push_back(cloud_9);

  if(cloud11->points.size() != 0)
    output_clouds.push_back(cloud11);
  if(cloud_11->points.size() != 0)
    output_clouds.push_back(cloud_11);

  if(cloud13->points.size() != 0)
    output_clouds.push_back(cloud13);
  if(cloud_13->points.size() != 0)
    output_clouds.push_back(cloud_13);

  if(cloud15->points.size() != 0)
    output_clouds.push_back(cloud15);
  if(cloud_15->points.size() != 0)
    output_clouds.push_back(cloud_15);

  // frame name needed to visualize in rviz
  for(auto cloudptr : output_clouds){
    cloudptr->header = point_cloud->header;
  }

}

void PanelDetectionNode::getClusteredCloudsFromPCLCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                                                        std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>& clustered_clouds,
                                                        int min_points, int max_points, float cluster_tolerance){

  std::vector<pcl::PointIndices> cluster_indices;

  if(input_cloud->points.size() < min_points)
    PCL_ERROR("Number of points in pointcloud smaller than minimum number of clustersize. Cannot cluster!");
  else{
    clustered_clouds.clear();
    //get the indicies of clustered Points
    getClusterIndicesfromPCLCloud(input_cloud, cluster_indices, min_points, max_points, cluster_tolerance);
    //make single Pointclouds based on clustered Clouds

    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end (); ++it){
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

      for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
        cloud_cluster->points.push_back (input_cloud->points.at(*pit));

      cloud_cluster->width = cloud_cluster->points.size ();
      cloud_cluster->height = 1;
      cloud_cluster->is_dense = true;

      //calculate geometric size of clustered cloud
      if(cloud_cluster->points.size()>5){
        pcl::PointXYZ point1 = cloud_cluster->points.at(0);
        pcl::PointXYZ point2 = cloud_cluster->points.back();

        double distance = calculatePCLPointDistance(point1, point2);

        if(distance < 1.1 && distance > 0.5){
          clustered_clouds.push_back(cloud_cluster);
        }
      }
    }
  }
}

void PanelDetectionNode::getClusterIndicesfromPCLCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                                                       std::vector<pcl::PointIndices>& output_indices,
                                                       int min_points, int max_points, float cluster_tolerance){

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (input_cloud);

  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance (cluster_tolerance);
  ec.setMinClusterSize (min_points);
  ec.setMaxClusterSize (max_points);
  ec.setSearchMethod (tree);
  ec.setInputCloud (input_cloud);
  ec.extract (output_indices);
}

double PanelDetectionNode::calculatePCLPointDistance(const pcl::PointXYZ& point1, const pcl::PointXYZ& point2){

  double length_vector;
  pcl::PointXYZ vec;

  vec.data[0] = point1.data[0] - point2.data[0];
  vec.data[1] = point1.data[1] - point2.data[1];
  vec.data[2] = point1.data[2] - point2.data[2];

  length_vector = sqrt(vec.data[0]*vec.data[0] + vec.data[1]*vec.data[1] + vec.data[2]*vec.data[2]);

  return length_vector;
}

